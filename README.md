# TDG instrumentation script
This is a script to combine Extrae's .prv and .pcf data in order to extract execution times, as well as papi event counter values and add them to the already formed TDG structures produced by the compiler as part of the tdg.dot files.

**Usage:**
*  Input arguments are: example_tdg_files example.prv example.pcf 
    * example_tdg_files must be either: .dot or .json (if e.g. example_tdg.dot is passed a new output file will be generated, while if example_tdg.json is passed this input file will be modified and containing tdg structure will be preserved, just new measurement results (from the new prv file) will be added to it.
    * (if not called from the same folder, absolute paths are needed)
    * Example: python parsePrvAndTdg.py example_tdg1.dot example_tdg2.dot example.prv example.pcf
*  Output will be .json file with the needed data.

Primary goal is to view different execution information, that happens during execution of an OpenMP task. Specifically: execution time of the task, but also PAPI event counters that happen during the execution of the observed task (like completed instructions, total cycles, cache misses, ...)
The execution time is always tracked, while types of PAPI event counters that need to be observed are filled dynamically (will be read from the .pcf file) and combined with the execution data, so that this script can be used for different extrae configurations and different settings of what needs to be observed.
Times of executions are shown in microseconds 'us'. While for papi events specific information and visualization, Paraver can be consulted.

Requirements: 
*  Python2 or higher
*  Being able to collect the valid input data:
   *  Compiling application that provides the tdg information in form of the tdg.dot file
   *  Running Extrae with the given application to get the execution traces
   *  OR
   *  Running Extrae with the given application to get the new execution traces, while already having existing tdg.json file for the tdg structure information
   *  OR
   *  Having already obtained .prv .pcf and tdg.dot (or tdg.json) files for the given application

Notes:
*  Extrae change is needed in order for this script to work correctly: extrae needs to have info about the tdg node ID when observing execution values, which requires small change in its code.
*  Papi counter values can be viewed and compared using Paraver, by loading the .prv file, then by choosing the cfg file to view the specific event (e.g. paraverHome/cfgs/counters\_PAPI/performance/cycles\_per\_us.cfg), 
and then by going to Windows Properties -> Semantic -> Thread and choose Next Evt Val which is the correct way to view PAPI events, as they are shown as accumulated values in the timespan between two events.




